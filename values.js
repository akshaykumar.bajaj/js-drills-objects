let get_key = require('./keys.js')
function values(obj) {
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values

    let ans = [];

    if (typeof obj != 'object')
        return ans;

    let key_arr = get_key(obj);

    for (let i = 0; i < key_arr.length; i++) {
        if (typeof key_arr[i] != 'function')
            ans.push(obj[key_arr[i]]);
    }

    return ans;
}
module.exports = values;