let get_keys = require('./keys.js');

function invert(obj) {
    // Returns a copy of the object where the keys have become the values and the values the keys.
    // Assume that all of the object's values will be unique and string serializable.
    // http://underscorejs.org/#invert
    let ans = {};

    if (typeof obj != 'object')
        return ans;

    let key_arr = get_keys(obj);

    for (let i = 0; i < key_arr.length; i++) {
        ans[obj[key_arr[i]]] = key_arr[i];
    }

    return ans;
}

module.exports = invert;