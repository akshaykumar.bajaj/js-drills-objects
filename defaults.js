let get_keys = require('./keys.js');

function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults

    if (typeof obj != 'object' || typeof defaultProps != 'object')
        return {};

    let key_arr = get_keys(defaultProps);

    for (let i = 0; i < key_arr.length; i++) {
        if (obj[key_arr[i]] === undefined)
            obj[key_arr[i]] = defaultProps[key_arr[i]];
    }

    return obj;
}

module.exports = defaults;