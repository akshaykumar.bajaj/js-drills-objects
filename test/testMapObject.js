let mapObject = require('../mapObject.js')

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const result = mapObject(testObject,function (val) { return val*val});

console.log(result);


console.log(mapObject(function (val) { return val*val}));

console.log(mapObject({},function (val) { return val*val}));

console.log(mapObject(10,function (val) { return val*val}));


console.log(mapObject());

console.log(mapObject({}));

console.log(mapObject(10));


console.log(mapObject(testObject));

console.log(mapObject(testObject,10));