function keys(obj) {
    // Retrieve all the names of the object's properties.
    // Return the keys as strings in an array.
    // Based on http://underscorejs.org/#keys
    if (typeof obj != 'object')
        obj = {};
    let ans = Object.getOwnPropertyNames(obj);
    return ans;

}

module.exports = keys;