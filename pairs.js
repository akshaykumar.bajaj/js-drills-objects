let get_keys = require('./keys.js');

function pairs(obj) {
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs
    let ans = [];

    if (typeof obj != 'object')
        return ans;

    let key_arr = get_keys(obj);
    let len = key_arr.length;

    for (let i = 0; i < len; i++) {
        ans.push([key_arr[i], obj[key_arr[i]]]);
    }
    return ans;
}

module.exports = pairs;