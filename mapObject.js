let get_keys = require('./keys.js');

function mapObject(obj, cb) {
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject

    if (typeof obj != 'object' || typeof cb != 'function')
        return {};

    let key_arr = get_keys(obj);
    let len = key_arr.length;

    for (let i = 0; i < len; i++) {
        obj[key_arr[i]] = cb(obj[key_arr[i]]);
    }
    return obj;
}

module.exports = mapObject;
